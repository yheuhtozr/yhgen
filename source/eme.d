import std.algorithm;
import std.range;
import std.conv;
debug { import std.stdio; }

alias i = std.range.iota;

uint LEN = 0; // dummy type flag
immutable ubyte INVSIZE = 8; // number of emes
immutable char[] EMES = ['J', 'U', 'Y', 'L', 'O', 'I', 'E', 'A']; // emes' mapping
immutable bool function(ubyte[]) validSeq = (s) => ! any!( (a) => a < 0 || a >= INVSIZE )(s);
immutable bool function(string) validExp = (x) => ! any!( (a) => !EMES.canFind(a) )(x);
immutable typeof(LEN) function(ubyte[]) seq2ord = (s)
{
  auto rs = s.dup.retro.array;
  // auto stack = 0;
  // foreach (i, e; rs) { stack += INVSIZE^^i * (e+1); }
  // return cast(typeof(LEN)) (stack - 1);
  return cast(typeof(LEN)) ( zip(i(0, rs.length), rs).map!((a) => INVSIZE^^a[0] * (a[1]+1)).sum - 1 );
};
immutable ubyte[] function(typeof(LEN)) ord2seq = (n)
{
  auto divd = n;
  int digits = 1;
  while (divd >= INVSIZE ^^ digits) { divd -= INVSIZE ^^ digits; digits++; }

  ubyte[] store = []; debug { writeln(divd, "=>",  store); }
  foreach (e; i(1, digits).retro)
  {
    auto q = divd / INVSIZE^^e;
    store ~= q.to!ubyte;
    divd = divd % INVSIZE^^e; debug { writeln(e, "/", store); }
  } debug { writeln(divd, "->", store); }
  return store ~= divd.to!ubyte;
};
immutable ubyte[] function(ubyte[], bool) revolve = (a, b) => b ? a[$-1] ~ a[0..$-1] : a[1..$] ~ a[0];
immutable typeof(LEN) function(typeof(LEN), bool) revolveN = (a, b) => seq2ord(revolve(ord2seq(a), b));

unittest
{
  assert(EMES.length == INVSIZE);
  assert(validSeq([0]));
  assert(!validSeq([10]));
  assert(validExp("AEIJLOUY"));
  assert(!validExp("ABCDEFG"));
  assert(seq2ord([0,0,0]) == 72);
  assert(ord2seq(72) == [0,0,0]);
}

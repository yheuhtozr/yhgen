import std.algorithm;
import std.array;
import std.conv;
import std.datetime.systime;
import std.random;
import std.range;
import std.stdio;
import std.string;
import std.regex;
import scriptlike.interact;
import betterNullable;
import eme;
import dic;

immutable(string) ver = "0.2.5";

Dict dict;

Nullable!(ubyte[2]) num_reader(string num)
{
	auto c = num.matchFirst( regex(`^(?:(\d*)(-))?(\d+)$`) );

	Nullable!(ubyte[2]) ret;
	if (c.empty)
		ret = null;
	else if (c[2] == "")
		ret = [c[3].to!ubyte, c[3].to!ubyte];
	else if (c[1] == "" || c[1].to!ubyte >= c[3].to!ubyte)
		ret = [0.to!ubyte, c[3].to!ubyte];
	else
		ret = [c[1].to!ubyte, c[3].to!ubyte];
	return ret;
}

ubyte[] eme_reader(string exp)
{
	if (!validExp(exp)) return [];
	return exp.map!( (a) => EMES.countUntil(a).to!ubyte ).array;
}

Nullable!string tryGet(string[] s, int e)
{
	if (e < s.length) return s[e].nullable;
	else return Nullable!string.init;
}

string buildWord(ubyte[] s) { return s.map!((a) => EMES[a]).array; }

void create(string[] com)
{
	ubyte[2] limit;
	ubyte[] initial;
	if (com.tryGet(1))
	{
		auto num = num_reader(com[1]);
		if (num != null && num[0] > 0) { limit = num; }
		else if (num[0..$].sum > 0) { limit = [1, num[1]]; } // make sure at least one char long
		else { writeln("Please input correct (positive) length. Try again."); return; }
	}
	else
	{
		writeln("Please input length. Try Again.");
		return;
	}
	if (com.tryGet(2)) initial = eme_reader(com[2]);

	ubyte[] slots = []; debug { ubyte[] buff = []; writeln(limit); }
	auto options = iota(limit[0], limit[1]+1).filter!((a) => dict.afford(a.to!ubyte, initial)).array; debug { writeln(options); }
	if (options.empty) { writeln("This section is full. Try again."); return; }
	do
	{
		auto len = choice(options);
		slots = generate!( () => choice(iota(0.to!ubyte, INVSIZE)) ).take(len).array;
	foreach (e; 0 .. min(slots.length, initial.length)) { slots[e] = initial[e]; } //debug { writeln("in: ", slots); buff = slots; }
	}
	while (dict.vacancy(slots)); debug { writeln("out: ", slots, buff); }

	auto wordform = slots.buildWord;
	if ( userInput!bool("Do you accept the word: " ~ wordform) )
	{
		if (dict.add(slots))
			writeln("Registered: " ~ wordform);
		else
			writeln("Registration failed. Try again.");
	}
	else
	{
		writeln("You rejected.");
	}
}

auto warnBlank = "Please specify wordform. Try again.", warnWrong = "Please correctly input wordform. Try again.";
void view(string[] com) { view(true, com); }
void view(bool all, string[] com)
{
	ubyte[2] limit = [0, 0];
	ubyte[] initial;
	if (com.tryGet(1))
	{
		auto num = num_reader(com[1]); debug { writeln("view() ", com[1]); }
		if (num != null)
		{
			limit = num;
			if (com.tryGet(2))
			{
				initial = eme_reader(com[2]);
				if (initial.length <= 0) { writeln(warnWrong); return; }
			}
			else { writeln(warnBlank); return; }
		}
		else
		{
			initial = eme_reader(com[1]);
			if (initial.length <= 0) { writeln(warnWrong); return; }
		}
	}
	else { writeln(warnBlank); return; } debug { writeln("view() ", limit, "/", initial); }

	auto list = (all ? [0, 1, -1] : [0]).map!(a => dict.list(initial, limit[0], limit[1], a.to!byte)).array; debug { writeln(list); }
	auto attrs = list.map!( a => a.map!(b => dict.attr(b)).array ).array;
	display(list, attrs);
}

void expel(string[] com)
{
	if (com.tryGet(1))
	{
		auto target = eme_reader(com[1]);
		if (target.empty) { writeln(warnWrong); return; }
		auto warnDelete = "Deletion canceled, or you failed to input \"DELETE\".";

		try
		{
			auto decision = userInput("If you really want to delete the word " ~ com[1] ~ ", input DELETE");
			if (decision == "DELETE")
			{
				if (dict.remove(target))
					writeln("Deleted: " ~ com[1]);
				else
					writeln("Deletion failed. Try again.");
			}
			else
				writeln(warnDelete);
		}
		catch (NoInputException e)
		{
			writeln(warnDelete); return;
		}
	}
	else
		writeln(warnBlank);
}

void hist(string[] com)
{
	if (com.tryGet(1))
	{
		typeof(LEN) l;
		try { l = com[1].to!(typeof(LEN)); } catch(ConvException e) { l = 0; }

		ubyte[][] res = [];
		if (l > 0) res = dict.history(l);
		else if (com[1] == "*") res = dict.history(0);
		else { writeln("Please input correct number or \"*\" for all."); return; }

		display(res);
	}
	else
	{
		writeln("Please input number or \"*\" for all.");
	}
}

void display(ubyte[][][] n, SysTime[][] t, bool flag = true)
{
	ubyte w; debug { writeln(n, t); }
	bool none = n.fold!((a, b) => a && b.empty)(true);
	if (flag) w = none ? 0.to!ubyte : n.join.map!((a) => a.length).maxElement.to!ubyte;
	if (flag) writeln("|");
	foreach (i, set; n)
	{
		if (!set.empty)
		{
			zip(set, t[i]).map!( (a) =>
				format(
					(flag ? "|" : "") ~ (!flag ? "" : [" ", ">", "<"][i]) ~ (flag ? "%-"~w.to!string~"s" : "%s") ~ " %s",
					a[0].buildWord,
					a[1].toUnixTime == 0 ? "N/A" : a[1].toISOExtString
				)
			).each!writeln;
		}
	}
	if (none) { writeln( (flag ? "| " : "") ~ "No entries found." ); }
	if (flag) writeln("|");
}
void display(ubyte[][] n, bool flag = true) { display([n, [], []], [n.map!((a) => dict.attr(a)).array, [], []], flag); }

void main()
{
	writeln("YhGen ver. " ~ ver ~ "\n");
	dict = new Dict();
	writeln("Usage:
	(g)ive/+ [3-]4 [JU]
	(c)heck[!]/*[!] [2-3] YL
	(o)ust/- AAAA
	(h)istory 100
	(e)xit/x
	");
	debug
	{
		ubyte[] slots = []; ubyte[] buff = [];
		do
		{
			ubyte[2] limit = [3, 3];
			ubyte[] initial = [0];
			auto len = choice(iota(limit[0], limit[1]+1));
			slots = generate!( () => choice(iota(0.to!ubyte, INVSIZE)) ).take(len).array;
			foreach (e; 0 .. min(slots.length, initial.length)) { slots[e] = initial[e]; } buff = slots; writeln("in: ", slots, buff);
		}
		while (dict.vacancy(slots)); writeln("out: ", slots, buff);
	}


	loop: while (true)
	{
		auto input = userInput("Please enter command...");
		auto command = input.strip.split;
		switch (command.tryGet(0))
		{
			case "give", "g", "+": create(command); continue;
			case "check!", "c!", "*!": view(false, command); continue;
			case "check", "c", "*": view(command); continue;
			case "oust", "o", "-": expel(command); continue;
			case "history", "h": hist(command); continue;
			case "exit", "e", "x": break loop;
			default: writeln("Please input correct command.");
		}
	}
	writeln("\nSee you next time!");
}

import std.algorithm;
import std.array;
import std.conv;
import std.datetime.systime;
import std.datetime.timezone;
import std.range;
import std.stdio;
import std.string;
import std.zip;
import scriptlike.file;
import scriptlike.path;
// import betterNullable;
import eme;


mixin template singleton()
// from http://www.codelogy.org/entry/2013/01/24/010106
{
  static typeof(this) opCall()
  {
    static typeof(this) instance;
    if (!instance) instance = new typeof(this)();
    return instance;
  }
}

// struct Attributes
// {
//   SysTime time;
//   bool frozen;
//
//   string marshal()
//   {
//     auto wTime = (SysTime a) => a.toISOString, wBool = (bool a) => a ? "1" : "0";
//     return [wTime(time), wBool(frozen)].join(' ');
//   }
//   void unmarshal(string[] data)
//   {
//     auto delegate(string) rTime = (a) => SysTime.fromISOString(a), rBool = (a) => a == "1" ? true : false;
//     foreach (i, e; data) // support shorter field size for backward compatibility
//     {
//       switch (i)
//       {
//         case 0: time = rTime(e);
//         case 1: frozen = rBool(e);
//         default;
//       }
//     }
//   }
// }

class Dict
{
  private ubyte[] chunks = [];
  private SysTime[typeof(LEN)] attrs;
  static private Path dict = Path("./dict.dat");
  private ubyte[] lw = [], rw = [];
  private typeof(LEN)[typeof(LEN)] llookup, rlookup;

  this()
  {
    if (chunks.empty && attrs.keys.empty) load(); debug { writeln(chunks.length); writeln(attrs); }
  }

  bool add(ubyte[] s) { return record(s, true); }

  bool remove(ubyte[] s) { return record(s, false); }

  bool vacancy(ubyte[] s)
  {
    if (!validSeq(s)) return false;
    auto id = seq2ord(s);
    return (bitGet(id, chunks) || bitGet(id, rw) || bitGet(id, lw));
  }

  bool afford(ubyte d, ubyte[] s = [])
  { debug { writeln("afford - d: ", d, " s: ", s); }
    if (s.length > d) return false;
    else if (s.length == d) return !vacancy(s);
    else
    {
      typeof(LEN) delegate(size_t) f = (n) => chunk(seq2ord(s ~ 0.to!ubyte.repeat(n-s.length).array));
      auto up = f(d + 1);
      return up > chunks.length || up > lw.length || up > rw.length || ! all!( (a) => (chunks[a] | lw[a] | rw[a]) == ubyte.max )(iota(f(d), up));
    }
  }

  ubyte[][] list(ubyte[] s, ubyte mn, ubyte mx, byte revo = 0)
  {
    auto lim = mx == 0 ? (typeof(LEN)).max : mx > mn ? mx+1 : mn+1;
    ubyte[][] store = [];
    auto lookup(typeof(LEN) num)
    {
      auto target = revo > 0 ? rlookup : llookup;
      if (num in target) store ~= ord2seq(target[num]);
    }

    foreach (long e; mn .. lim) // biggest possible signed type for safety
    {
      auto d = e - s.length;
      if (d < 0) continue;

      auto start = s ~ array(0.to!ubyte.repeat(cast(size_t) d)), end = s ~ array(7.to!ubyte.repeat(cast(size_t) d));
      if (start == end)
      {
        if (revo == 0) { if ( bitGet(seq2ord(start), chunks) ) store ~= start; }
        else { lookup(seq2ord(start)); }
      }
      else
      {
        auto sChunk = chunk(seq2ord(start)), eChunk = chunk(seq2ord(end));
        if (sChunk >= chunks.length) break;
        foreach (ch; sChunk .. eChunk+1)
        {
          auto head = first(ch).to!(typeof(LEN));
          if (revo == 0)
          {
            auto prefix = ord2seq(head)[0..$-1]; // discard the lowest unit
            auto chunk = chunkGet(ch, chunks);
            foreach (i, b; chunk) { if (b) store ~= prefix ~ [i.to!ubyte]; } debug { writeln(e, ":", ch, " - ", prefix); }
          }
          else
          {
            foreach (i; 0 .. 8) { lookup(head + i); }
          }
        }
      }
    }
    return store;
  }

  ubyte[][] history(typeof(LEN) n)
  {
    auto f = attrs.keys.filter!((a) => bitGet(a, chunks)).array;
    // means newest n
    auto f_ = (n > 0 && n < f.length-1) ? f.topN!((a, b) => attrs[a] > attrs[b])(n.to!size_t) : f;
    // sort in ascending order
    auto s =  f_.sort!((a, b) => attrs[a] < attrs[b]).array; debug { writeln(s); }
    return s.map!ord2seq.array;
  }

  SysTime attr(ubyte[] s) // systime conflicts with betterNullable using typecons internally?
  {
    auto ord = seq2ord(s);
    return (ord in attrs) ? attrs[ord] : SysTime.fromUnixTime(0);
  }

  private:
  bool bitSet(typeof(LEN) offset, bool b, ref ubyte[] dest)
  {
    ubyte data;
    auto ch = chunk(offset), i = 7 - chunkIndex(offset);
    if (ch >= dest.length) { dest ~= 0.to!ubyte.repeat( ch - dest.length + 1 ).array; }
    data = dest[ch];
    dest[ch] = ( b ? (data | 1 << i) : (data & ~(1 << i)) ).to!ubyte;
    return true;
  }
  bool bitGet(typeof(LEN) offset, in ubyte[] dest) { return chunkGet(chunk(offset), dest)[chunkIndex(offset)]; }
  bool[] chunkGet(typeof(LEN) ch, in ubyte[] dest)
  {
    ubyte chunk;
    chunk = ch < dest.length ? dest[ch] : 0;
    return iota(8).map!( (a) => (chunk & 1 << (7-a)) == 2^^(7-a) ).array;
  }
  auto chunk(typeof(LEN) n) { return n / 8; }
  auto chunkIndex(typeof(LEN) n) { return n % 8; }
  auto first(typeof(LEN) ch) { return ch * 8; }
  bool record(ubyte[] s, bool on)
  {
    if (!validSeq(s)) return false;
    auto sr = revolve(s, true), sl = revolve(s, false);
    auto ord = seq2ord(s), orr = seq2ord(sr), orl = seq2ord(sl);
    if ((bitGet(ord, chunks) || bitGet(orr, rw) || bitGet(orl, lw)) == on) return false;
    auto result = bitSet(ord, on, chunks), rr = bitSet(orr, on, rw), rl = bitSet(orl, on, lw);
    if (result && rr && rl)
    {
      auto t = Clock.currTime;
      immutable auto tz = new SimpleTimeZone(t.utcOffset); // generate it every time!?
      attrs[ord] = t.toOtherTZ(tz);
      save();
      if (on)
      {
        rlookup[orr] = ord;
        llookup[orl] = ord;
      }
      else
      {
        rlookup.remove(orr);
        llookup.remove(orl);
      }
      return true;
    }
    else return false;
  }

  void load()
  {
    if (!exists(dict))
    {
      writeln("Data file not found!");
      return;
    }
    auto data = new ZipArchive(read(dict));
    chunks = data.expand(data.directory["chunks"]); debug { writeln(data.expand(data.directory["attrs"])); }
    foreach (i, e; iota(0, chunks.length).map!((a) => chunkGet(a, chunks)).join)
    {
      if (e)
      {
        auto r = revolveN(i, true), l = revolveN(i, false);
        bitSet(r, true, rw);
        bitSet(r, true, lw);
        rlookup[r] = i;
        llookup[l] = i;
      }
    }
    data.expand(data.directory["attrs"]).split('\n').map!( (e) => e.split(' ') )
      .each!( (e) =>
        attrs[(cast(string) e[0]).to!(typeof(LEN))] = SysTime.fromISOString( (cast(string) e[1]).strip )
      );

      debug
      {
        auto t = Clock.currTime;
        writeln(t); writeln(t.utcOffset);
      }
  }
  void save()
  {
    // https://dlang.org/phobos/std_zip.html
    ArchiveMember am = new ArchiveMember();
    am.name = "chunks";
    am.expandedData(chunks);
    am.compressionMethod(CompressionMethod.deflate);
    ArchiveMember am2 = new ArchiveMember();
    am2.name = "attrs";
    auto attrs_s = cast(ubyte[][]) attrs.keys.map!( (k) => k.to!string ~ [' '] ~ attrs[k].toISOString ).array;
    am2.expandedData(attrs_s.join('\n'));
    am2.compressionMethod(CompressionMethod.deflate);
    ZipArchive zip = new ZipArchive();
    zip.addMember(am);
    zip.addMember(am2);
    void[] compressed_data = zip.build;
    writeFile(dict, compressed_data);
  }

  mixin singleton;

  unittest
  {
    ArchiveMember test = new ArchiveMember();
    ubyte[] arr = [0,1,2,3,4];
    test.expandedData(arr);
    assert(test.expandedSize == 5);
  }
}
